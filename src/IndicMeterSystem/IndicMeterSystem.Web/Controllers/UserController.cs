﻿using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с пользователями.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Конструктор контроллера пользователей.
        /// </summary>
        /// <param name="userService">Объект интерфейса по работе с пользователями.</param>
        public UserController(IUserService userService)
        {
            _userService = userService;
        }


        /// <summary>
        /// Добавляет пользователя.
        /// </summary>
        /// <param name="user">Данные пользователя.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddUser([FromBody] UserModel user)
        {
            await _userService.AddUserAsync(user);
            return Ok();
        }

    }
}
