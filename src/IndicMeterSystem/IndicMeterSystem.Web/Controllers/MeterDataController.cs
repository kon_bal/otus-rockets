﻿using System.Threading.Tasks;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с показаниями.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class MeterDataController : ControllerBase
    {
        private readonly IMeterDataService _meterDataService;

        /// <summary>
        /// Конструктор контроллера показаний.
        /// </summary>
        /// <param name="meterDataService">Объект интерфейса по работе с показаниями.</param>
        public MeterDataController(IMeterDataService meterDataService)
        {
            _meterDataService = meterDataService;
        }

        /// <summary>
        /// Добавляет новую запись показания.
        /// </summary>
        /// <param name="meterDataModel">Модель данных показания.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddMeterData(MeterDataModel meterDataModel)
        {
            ValidateModel(meterDataModel);

            await _meterDataService.AddAsync(meterDataModel);

            return Ok();
        }

        /// <summary>
        /// Получает данные по показанию с данными по платежам.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возсращает модель показания со списком платежей.</returns>
        [HttpGet("{meterDataId}")]
        public async Task<IActionResult> GetById(int meterDataId)
        {
            var meterDataModel = await _meterDataService.GetByIdAsync(meterDataId);

            return Ok(meterDataModel);
        }

        /// <summary>
        /// Производит валидацию данных.
        /// </summary>
        /// <param name="meterDataModel">Модель показания.</param>
        private static void ValidateModel(MeterDataModel meterDataModel)
        {
            if (meterDataModel == null)
            {
                throw new ValidationException(nameof(meterDataModel), "Argument null exception.");
            }

            foreach (var meterValue in meterDataModel.MeterValues)
            {
                if (meterValue.Value == default)
                {
                    throw new ValidationException(nameof(meterValue.Value), 
                        "Argument \"value\" cannot be zero.");
                }

                if (meterValue.Number < 1)
                {
                    throw new ValidationException(nameof(meterValue.Number), 
                        "Argument \"number\" cannot be less than one.");
                }
            }
        }
    }
}
