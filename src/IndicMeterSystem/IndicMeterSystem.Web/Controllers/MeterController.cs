﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MeterController : ControllerBase
    {
        private readonly IMeterService _meterService;

        public MeterController(IMeterService meterService)
        {
            _meterService = meterService;
        }

        /// <summary>
        /// Метод получает счетчики по id адреса.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает список счетчиков</returns>
        [HttpGet("address/{addressId}")]
        public async Task<IEnumerable<MeterModel>> GetMeter(int addressId)
        {
            var meters = await _meterService.GetByAddressIdAsync(addressId);
            return meters;
        }

        /// <summary>
        /// Добавление счетчика
        /// </summary>
        /// <param name="meter"></param>
        /// <returns>status</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddMeter([FromBody] MeterModel meter)
        {
            await _meterService.AddMeterAsync(meter);
            return Ok();
        }
    }
}
