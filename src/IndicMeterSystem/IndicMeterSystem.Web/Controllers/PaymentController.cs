﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с платежами.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;

        /// <summary>
        /// Конструктор контроллера платежей.
        /// </summary>
        /// <param name="paymentService">Объект интерфейса по работе с платежами.</param>
        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        /// <summary>
        /// Добавляет новую запись платежа.
        /// </summary>
        /// <param name="paymentModel">Модель данных платежа.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> Add(PaymentModel paymentModel)
        {
            if (paymentModel == null)
            {
                throw new ValidationException(nameof(paymentModel), "Argument null exception.");
            }

            await _paymentService.AddAsync(paymentModel);

            return Ok();
        }

        /// <summary>
        /// Получает список платежей по идентификатору показания.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает список платежей.</returns>
        [HttpGet("meter/{meterDataId}")]
        public async Task<IEnumerable<PaymentModel>> Get(int meterDataId)
        {
            var payment = await _paymentService.GetByMeterDataIdAsync(meterDataId);
            return payment;
        }
    }
}
