﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Методы для работы с подтарифами
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SubTariffController : ControllerBase
    {
        private readonly ISubTariffService _subTariffService;

        public SubTariffController(ISubTariffService subTariffService)
        {
            _subTariffService = subTariffService;
        }

        /// <summary>
        /// Получает список подтарифов по Id тарифа
        /// </summary>
        /// <param name="tariffId">Id тарифа</param>
        /// <returns>Список подтарифов</returns>
        [HttpGet("tariff/{tariffId}")]
        public async Task<IEnumerable<SubTariffModel>> GetSubTariff(int tariffId)
        {
            return await _subTariffService.GetByTariffIdAsync(tariffId);
        }
    }
}
