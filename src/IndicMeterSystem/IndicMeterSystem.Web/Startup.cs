using System;

using AutoMapper;

using IndicMeterSystem.Data.EF;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Data.Repositories;
using IndicMeterSystem.Services.Services;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services;
using IndicMeterSystem.Web.Infrastructure.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;

namespace IndicMeterSystem.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // получаем строку подключения из файла конфигурации
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddSwaggerGen(c =>
            {
                c.IncludeXmlComments(GetXmlCommentsPath());
            });

            // добавляем контекст в качестве сервиса в приложение
            services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(connection));

            // добавляем связи интерфейсов с реализацией
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IMeterDataService, MeterDataService>();
            services.AddScoped<IMeterService, MeterService>();
            services.AddScoped<ISubTariffService, SubTariffService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<ITariffTypeService, TariffTypeService>();
            services.AddScoped<IUserService, UserService>();

            // настраиваем automapper
            services.AddAutoMapper(typeof(AutoMapperProfile));

            // используем контроллеры без представлений
            services.AddControllers();
        }

        private static string GetXmlCommentsPath() => $@"{AppDomain.CurrentDomain.BaseDirectory}\Swagger.XML";

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
	        app.UseExceptionHandling();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");
            });

            app.UseRouting();

            // подключаем маршрутизацию на контроллеры
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
