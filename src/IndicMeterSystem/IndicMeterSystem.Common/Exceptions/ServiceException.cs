﻿using System;
using System.Net;

namespace IndicMeterSystem.Common.Exceptions
{
	/// <summary>
	/// Класс для исключений об ошибке сервиса при обработке запроса.
	/// </summary>
	public class ServiceException : AppException
	{
		/// <summary>
		/// Создает экземпляр класса <see cref="ValidationException"/>.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <param name="innerException">Дополнительное исключение.</param>
		public ServiceException(string errorCode, Exception innerException = null)
			: base(errorCode, HttpStatusCode.InternalServerError, innerException)
		{
		}

		/// <summary>
		/// Создает экземпляр класса <see cref="ValidationException"/>.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <param name="message">Сообщение об ошибке.</param>
		/// <param name="innerException">Дополнительное исключение.</param>
		public ServiceException(string errorCode, string message, Exception innerException = null)
			: base(errorCode, message, HttpStatusCode.InternalServerError, innerException)
		{
		}
	}
}
