﻿using System;
using System.Net;

namespace IndicMeterSystem.Common.Exceptions
{
	/// <summary>
	/// Класс для исключений о не найденных данных.
	/// </summary>
	public class NotFoundException : AppException
	{
		/// <summary>
		/// Создает экземпляр класса <see cref="ValidationException"/>.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <param name="innerException">Дополнительное исключение.</param>
		public NotFoundException(string errorCode, Exception innerException = null)
			: base(errorCode, HttpStatusCode.NotFound, innerException)
		{
		}

		/// <summary>
		/// Создает экземпляр класса <see cref="ValidationException"/>.
		/// </summary>
		/// <param name="errorCode">Код ошибки.</param>
		/// <param name="message">Сообщение об ошибке.</param>
		/// <param name="innerException">Дополнительное исключение.</param>
		public NotFoundException(string errorCode, string message, Exception innerException = null)
			: base(errorCode, message, HttpStatusCode.NotFound, innerException)
		{
		}
	}
}
