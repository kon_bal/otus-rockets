﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Модель с данными показания.
    /// </summary>
    public class MeterDataModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата добавления.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Список значений показаний.
        /// </summary>
        public IEnumerable<MeterValueModel> MeterValues { get; set; }

        /// <summary>
        /// Идентификатор счетчика.
        /// </summary>
        public int MeterId { get; set; }
    }
}
