﻿namespace IndicMeterSystem.Services.Models
{
    public class UserModel
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }


#nullable enable
        /// <summary>
        /// Электронная почта.
        /// </summary>
        public string? Email { get; set; }
    }
}