﻿namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Модель с данными типа тарифа
    /// </summary>
    public class TariffTypeModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Название типа
        /// </summary>
        public string Title { get; set; }
    }
}
