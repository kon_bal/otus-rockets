﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IndicMeterSystem.Services.Models
{
    public class MeterModel
    {
        /// <summary>
        /// Id счетчика
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Серийный номер
        /// </summary>
        public int SerialNumber { get; set; }
        /// <summary>
        /// Id адреса
        /// </summary>
        public int AddressId { get; set; }
        /// <summary>
        /// Описание счетчика
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Дата установки
        /// </summary>
        public DateTime DateFrom { get; set; }
        /// <summary>
        /// Дата замены
        /// </summary>
        public DateTime? DateTo { get; set; }
        /// <summary>
        /// Id тарифного плана
        /// </summary>
        public int TarrifId { get; set; }
    }
}
