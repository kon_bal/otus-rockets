﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Реализует интерфейс подтарифов
    /// </summary>
    public class SubTariffService : ISubTariffService
    {
        private readonly IRepository<SubTariff> _subTariffRepository;
        private readonly IMapper _mapper;

        public SubTariffService(IRepository<SubTariff> subTariffRepository, IMapper mapper)
        {
            _subTariffRepository = subTariffRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<SubTariffModel>> GetByTariffIdAsync(int tariffId)
        {
            var all = await _subTariffRepository.GetOnConditionAsync(p => p.TariffId == tariffId);
            if (all == null) return null;
            var subTariffData = all.Where(p => p.AddDate == all.Max(p => p.AddDate));
            var subTariffModel = _mapper.Map<IEnumerable<SubTariffModel>>(subTariffData);
            return subTariffModel;
        }
    }
}
