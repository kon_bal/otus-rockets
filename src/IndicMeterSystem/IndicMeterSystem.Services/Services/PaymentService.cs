﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Реализует интерфейс платежей
    /// </summary>
    public class PaymentService : IPaymentService
    {
        private readonly IRepository<Payment> _paymentRepository;
        private readonly ISubTariffService _subTariffService;
        private readonly IMapper _mapper;

        public PaymentService(
            IRepository<Payment> paymentRepository,
            ISubTariffService subTariffService,
            IMapper mapper)
        {
            _paymentRepository = paymentRepository;
            _subTariffService = subTariffService;
            _mapper = mapper;
        }

        public async Task AddAsync(PaymentModel paymentModel)
        {
            var payment = _mapper.Map<PaymentModel, Payment>(
                paymentModel,
                opts => opts.AfterMap((s, d) =>
                {
                    d.Date = DateTime.Now;
                }));

            await _paymentRepository.AddAsync(payment);
        }

        public async Task<IEnumerable<PaymentModel>> GetByMeterDataIdAsync(int meterDataId)
        {
            var paymentData = await _paymentRepository
                .GetOnConditionAsync(p => p.MeterDataId == meterDataId);

            var paymentModel = _mapper.Map<IEnumerable<PaymentModel>>(paymentData);

            return paymentModel;
        }

        public async Task<double> GetPaidAmount(int meterDataId)
        {
            var sum = (await GetByMeterDataIdAsync(meterDataId))
                .Sum(x => x.Amount);

            return sum;
        }

        public async Task<double> GetTotalSum(int tariffId, IEnumerable<MeterValueModel> meterValues)
        {
            var tariff = await _subTariffService.GetByTariffIdAsync(tariffId);

            double totalSum = default;

            foreach (var item in meterValues)
            {
                var tarifSum = tariff
                    .Where(x => x.Number == item.Number)
                    .Single();

                totalSum += item.Value * tarifSum.Price;
            }

            return totalSum;
        }
    }
}
