﻿using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    public class MeterService : IMeterService
    {
        private readonly IRepository<Meter> _meterRepository;

        public MeterService(IRepository<Meter> meterRepository)
        {
            _meterRepository = meterRepository;
        }

        /// <summary>
        /// Метод получает счетчики по id адреса.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает список счетчиков</returns>
        public async Task<IEnumerable<MeterModel>> GetByAddressIdAsync(int addressId)
        {
            var meter = await _meterRepository.GetOnConditionAsync(m => m.AddressId == addressId);
            var meterModel = meter
                .Select(m => new MeterModel()
                {
                    Id = m.Id,
                    SerialNumber = m.SerialNumber,
                    Description = m.Description,
                    DateFrom = m.DateFrom,
                    DateTo = m.DateTo
                }
                );

            return meterModel;
        }
        /// <summary>
        /// Добавление счетчика
        /// </summary>
        /// <param name="meter"></param>
        public async Task AddMeterAsync(MeterModel meter)
        {
            Meter m = new Meter()
            {
                AddressId = meter.AddressId,
                DateFrom = meter.DateFrom,
                DateTo = meter.DateTo,
                Description = meter.Description,
                SerialNumber = meter.SerialNumber,
                TarrifId = meter.TarrifId
            };

            await _meterRepository.AddAsync(m);
        }
    }
}
