﻿using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    public class AddressService : IAddressService
    {
        private readonly IRepository<Address> _addressRepository;

        public AddressService(IRepository<Address> addressRepository)
        {
            _addressRepository = addressRepository;
        }

 
        public async Task<IEnumerable<AddressModel>> GetByUserIdAsync(int userId)
        {
            var address = await _addressRepository.GetOnConditionAsync(m => m.UserId == userId);
            var addressModel = address                
                .Select(m => new AddressModel()
                {
                    Id = m.Id,
                    RegistrationAddress = m.RegistrationAddress,
                    Name = m.Name,
                    Deleted = m.Deleted,
                    Meters = m.Meters
                });
            return addressModel;
        }


        public async Task AddAddressAsync(AddressModel addrModel)
        {
            Address addr = new Address()
            {
                UserId = addrModel.UserId,
                RegistrationAddress = addrModel.RegistrationAddress,
                Name = addrModel.Name,
                Deleted = addrModel.Deleted
            };
            await _addressRepository.AddAsync(addr);
        }


        public async Task DeleteAddressAsync(int addressId)
        {
            var address = await _addressRepository.GetAsync(addressId);
            address.Deleted = true;
            await _addressRepository.UpdateAsync(address);
        }

    }
}
