﻿using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }


        public async Task AddUserAsync(UserModel user)
        {
            User u = new User()
            {
                UserName = user.UserName.Trim(),
                Login = user.Login.Trim(),
                Password = user.Password.Trim(),
                Email = user.Email.Trim()
            };
            var userExist = await _userRepository.AnyAsync(x =>
                x.Login.ToLower() == u.Login.ToLower() &&
                x.Password.ToLower() == u.Password.ToLower());
            if (userExist)
            {
                throw new ValidationException($"Пользователь с учетными данными {u.Login}/{u.Password} уже зарегистрирован !");
            }

            await _userRepository.AddAsync(u);
        }


        public async Task<UserModel> GetUserByLoginAsync(string login, string password)
        {
            login = login.Trim();
            password = password.Trim();
            var user = await _userRepository.GetOnConditionAsync(x => 
                x.Login.ToLower() == login.ToLower() && 
                x.Password.ToLower() == password.ToLower());
            if (user == null)
            {
                throw new ValidationException($"Пользователь с учетными данными {login}/{password} не зарегистрирован!");
            }     
            var userModel = user.Select(x => new UserModel()
            {
                Id = x.Id,
                UserName = x.UserName,
                Login = x.Login,
                Password = x.Password,
                Email = x.Email
            }).Single();

            return userModel; 
        }

    }
}
