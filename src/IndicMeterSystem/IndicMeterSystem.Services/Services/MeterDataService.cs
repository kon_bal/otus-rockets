﻿using System;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Реализует интерфейс показаний. 
    /// </summary>
    public class MeterDataService : IMeterDataService
    {
        private readonly IRepository<MeterData> _meterDataRepository;
        private readonly IMapper _mapper;

        public MeterDataService(
            IRepository<MeterData> meterDataRepository,
            IMapper mapper)
        {
            _meterDataRepository = meterDataRepository;
            _mapper = mapper;
        }

        public async Task AddAsync(MeterDataModel meterDataModel)
        {
            // Проверка на существование показания в текущем месяце.
            var isExist = await ExistAsync(
                DateTime.Now,
                meterDataModel.MeterId);

            if (isExist)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS30001));
            }

            // Находим прошлое значение показания.
            var oldMeterData = (await GetByDateAsync(
                meterDataModel.Date.AddMonths(-1),
                meterDataModel.MeterId));

            foreach (var meterValue in meterDataModel.MeterValues)
            {
                var meterValueNew = oldMeterData?.MeterValues
                    .Where(x => x.Number == meterValue.Number)
                    .Single();

                var difference = meterValue.Value - (meterValueNew?.Value ?? 0);

                if (difference < 0)
                {
                    throw new ServiceException(nameof(ErrorMessages.IMS30002));
                }

                meterValue.Difference = difference;
            }            

            var meterData = _mapper.Map<MeterDataModel, MeterData>(
                meterDataModel,
                opts => opts.AfterMap((s, d) =>
                {
                    d.Date = DateTime.Now;
                }));

            await _meterDataRepository.AddAsync(meterData);
        }

        public async Task<MeterDataWithPaymentsModel> GetByIdAsync(int meterDataId)
        {
            var meterData = (await _meterDataRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(MeterDataWithPaymentsModel.Payments),
                    nameof(MeterDataWithPaymentsModel.MeterValues),
                    x => x.Id == meterDataId))
                .FirstOrDefault();

            var meterDataModel = _mapper.Map<MeterDataWithPaymentsModel>(meterData);

            return meterDataModel;
        }

        public async Task<bool> ExistAsync(DateTime date, int meterId)
        {
            var dateStart = new DateTime(date.Year, date.Month, 1);
            var dateEnd = dateStart.AddMonths(1);

            var isExist = await _meterDataRepository
                .AnyAsync(x => x.Date < dateEnd
                    && x.Date >= dateStart
                    && x.MeterId == meterId);

            return isExist;
        }

        /// <summary>
        /// Получает показания в соответствии с параметрами.
        /// </summary>
        /// <param name="date">Период поиска.</param>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает список моделей.</returns>
        public async Task<MeterDataModel> GetByDateAsync(
            DateTime date, int meterId)
        {
            var dateStart = new DateTime(date.Year, date.Month, 1);
            var dateEnd = dateStart.AddMonths(1);

            // В течении месяца показания вносятся один раз.
            var meterDatas = (await _meterDataRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(MeterDataModel.MeterValues),
                    x => x.Date < dateEnd
                        && x.Date >= dateStart
                        && x.MeterId == meterId))
                .FirstOrDefault();

            return _mapper.Map<MeterDataModel>(meterDatas);
        }
    }
}
