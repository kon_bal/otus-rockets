﻿using AutoMapper;

using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<MeterData, MeterDataModel>().ReverseMap();

            CreateMap<Payment,PaymentModel>().ReverseMap();

            CreateMap<TariffType, TariffTypeModel>();

            CreateMap<SubTariff, SubTariffModel>();

            CreateMap<MeterData, MeterDataWithPaymentsModel>();
            
            CreateMap<MeterData, MeterDataWithSumModel>();
            
            CreateMap<MeterValue, MeterValueModel>().ReverseMap();
        }
    }
}
