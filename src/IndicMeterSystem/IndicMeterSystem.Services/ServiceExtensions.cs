﻿using System.Threading.Tasks;

using IndicMeterSystem.Common.Exceptions;

namespace IndicMeterSystem.Services
{
	public static class ServiceExtensions
	{
		/// <summary>
		/// Проверяет, что элемент существует.
		/// </summary>
		/// <typeparam name="TItem">Тип элемента.</typeparam>
		/// <param name="item">Элемент (операция)</param>
		/// <param name="errorCode">КОд ошибки.</param>
		/// <returns>Элемент.</returns>
		public static async Task<TItem> EnsureExists<TItem>(
			this Task<TItem> item,
			string errorCode)
			where TItem : class
		{
			var value = await item;

			if (value == null)
				throw new NotFoundException(errorCode);

			return value;
		}

		/// <summary>
		/// Проверяет, что элемент существует.
		/// </summary>
		/// <typeparam name="TItem">Тип элемента.</typeparam>
		/// <param name="item">Элемент (операция)</param>
		/// <param name="errorCode">КОд ошибки.</param>
		/// <returns>Элемент.</returns>
		public static TItem EnsureExists<TItem>(
			this TItem item,
			string errorCode)
			where TItem : class
		{
			if (item == null)
				throw new NotFoundException(errorCode);

			return item;
		}
	}
}
