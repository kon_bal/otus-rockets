﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с подтарифами
    /// </summary>
    public interface ISubTariffService
    {
        /// <summary>
        /// Получаем подтарифы по Id тарифа
        /// </summary>
        /// <param name="tariffId">Id тарифа</param>
        /// <returns>Список подтарифов указанного тарифа</returns>
        Task<IEnumerable<SubTariffModel>> GetByTariffIdAsync(int tariffId);

    }
}
