﻿using System;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с показаниями.
    /// </summary>
    public interface IMeterDataService
    {
        /// <summary>
        /// Добавляет новую запись показания.
        /// </summary>
        /// <param name="meterDataModel">Модель с данными по показанию.</param>
        Task AddAsync(MeterDataModel meterDataModel);

        /// <summary>
        /// Получает данные показаний счетчика на определенный период.
        /// </summary>
        /// <param name="date">Дата для поиска показания.</param>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает модель данных.</returns>
        Task<MeterDataModel> GetByDateAsync(DateTime date, int meterId);

        /// <summary>
        /// Получает данные по показанию и его платежи.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает модель показания со списком платежей.</returns>
        Task<MeterDataWithPaymentsModel> GetByIdAsync(int meterDataId);

        /// <summary>
        /// Проверяет наличие записи показания за период.
        /// </summary>
        /// <param name="date">Период на который идет поиск.</param>
        /// <param name="meterId">Идентификатор счетчика.</param>
        /// <returns>Возвращает true если значение найдено, иначе false.</returns>
        Task<bool> ExistAsync(DateTime date, int meterId);
    }
}
