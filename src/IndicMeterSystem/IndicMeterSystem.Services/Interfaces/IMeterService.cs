﻿using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Interfaces
{
    public interface IMeterService
    {
        /// <summary>
        /// Метод получает счетчики по id адреса.
        /// </summary>
        /// <param name="addressId">Идентификатор адреса.</param>
        /// <returns>Возвращает список счетчиков</returns>
        Task<IEnumerable<MeterModel>> GetByAddressIdAsync(int addressId);

        /// <summary>
        /// Добавление счетчика
        /// </summary>
        /// <param name="meter"></param>
        Task AddMeterAsync(MeterModel meter);
    }
}
