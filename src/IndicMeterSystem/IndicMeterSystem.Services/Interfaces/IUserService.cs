﻿using IndicMeterSystem.Services.Models;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Получение пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Данные пользователя.</returns>
        Task<UserModel> GetUserByLoginAsync(string login, string password);

        /// <summary>
        /// Добавление пользователя.
        /// </summary>
        /// <param name="user">Данные пользователя.</param>
        Task AddUserAsync(UserModel user);
    }
}
