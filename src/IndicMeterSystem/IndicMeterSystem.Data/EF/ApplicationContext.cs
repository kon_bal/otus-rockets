﻿using System.Reflection;

using IndicMeterSystem.Data.Entities;

using Microsoft.EntityFrameworkCore;

namespace IndicMeterSystem.Data.EF
{
    public class ApplicationContext : DbContext
    {
        public DbSet<MeterData> MeterDatas { get; set; }
        public DbSet<Meter> Meters { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<SubTariff> SubTariffs { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<TariffType> TariffTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Adresses { get; set; }
        public DbSet<MeterValue> MeterValues { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Заполняем таблицу типов тарифов фиксированными значениями
            modelBuilder.Entity<TariffType>().HasData(
                new TariffType[]
                {
                    new TariffType { Id = 1, Title = "Фиксированный платеж"},
                    new TariffType { Id = 2, Title = "По показаниям, однотарифный"},
                    new TariffType { Id = 3, Title = "По показаниям, двухтарифный"}
                });

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
