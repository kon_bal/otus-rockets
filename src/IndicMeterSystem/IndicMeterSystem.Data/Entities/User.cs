﻿using System.Collections.Generic;

namespace IndicMeterSystem.Data.Entities
{
    public class User
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Свойство навигации EF.
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

#nullable enable
        /// <summary>
        /// Электронная почта.
        /// </summary>
        public string? Email { get; set; }
    }
}