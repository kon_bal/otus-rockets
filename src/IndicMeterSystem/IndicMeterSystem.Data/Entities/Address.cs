﻿using System.Collections.Generic;

namespace IndicMeterSystem.Data.Entities
{
    public class Address
    {
        /// <summary>
        /// Идентификатор адреса.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Адрес пользователя.
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// Уточняющая информащая к адресу пользователя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Признак актуальности адреса.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Свойство навигации EF.
        /// </summary>		
        public User User { get; set; }

        /// <summary>
        /// Список связанных счетчиков.
        /// </summary>
        public IEnumerable<Meter> Meters { get; set; }
    }
}
