﻿namespace IndicMeterSystem.Data.Entities
{
    /// <summary>
    /// Содержит значения показаний.
    /// </summary>
    public class MeterValue
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Порядковый номер. Помогает различать записи при множественных показаниях.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Значение показания.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Разница с прошлым показанием.
        /// </summary>
        public int Difference { get; set; }

        /// <summary>
        /// Связь с общей информацией показания.
        /// </summary>
        public int MeterDataId { get; set; }

        /// <summary>
        /// Ссылка на связанную запись в показаниях.
        /// </summary>
        public MeterData MeterData { get; set; }
    }
}
