﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class Subtariffs_AdddatePrice_Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "SubTariffs",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "SubTariffs",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldDefaultValue: new DateTime(2020, 9, 18, 17, 12, 46, 488, DateTimeKind.Local).AddTicks(36));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Price",
                table: "SubTariffs",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "SubTariffs",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(2020, 9, 18, 17, 12, 46, 488, DateTimeKind.Local).AddTicks(36),
                oldClrType: typeof(DateTime));
        }
    }
}
