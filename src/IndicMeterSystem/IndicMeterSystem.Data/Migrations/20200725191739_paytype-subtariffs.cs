﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class paytypesubtariffs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_SubTariffs_PayTypeId",
                table: "SubTariffs",
                column: "PayTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubTariffs_PayTypes_PayTypeId",
                table: "SubTariffs",
                column: "PayTypeId",
                principalTable: "PayTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubTariffs_PayTypes_PayTypeId",
                table: "SubTariffs");

            migrationBuilder.DropIndex(
                name: "IX_SubTariffs_PayTypeId",
                table: "SubTariffs");
        }
    }
}
