﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class tariffsubtariffs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_SubTariffs_TariffId",
                table: "SubTariffs",
                column: "TariffId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubTariffs_Tariffs_TariffId",
                table: "SubTariffs",
                column: "TariffId",
                principalTable: "Tariffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubTariffs_Tariffs_TariffId",
                table: "SubTariffs");

            migrationBuilder.DropIndex(
                name: "IX_SubTariffs_TariffId",
                table: "SubTariffs");
        }
    }
}
