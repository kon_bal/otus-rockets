﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class fill_tarifftypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TariffTypes",
                columns: new[] { "Id", "Title" },
                values: new object[,]
                {
                    { 1, "Фиксированный платеж" },
                    { 2, "По показаниям, однотарифный" },
                    { 3, "По показаниям, двухтарифный" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TariffTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TariffTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TariffTypes",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
