﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class RenameIndicatorToMeterData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Indicators_IndicatorId",
                table: "Payments");

            migrationBuilder.DropTable(
                name: "Indicators");

            migrationBuilder.DropIndex(
                name: "IX_Payments_IndicatorId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "IndicatorId",
                table: "Payments");

            migrationBuilder.AddColumn<int>(
                name: "MeterDataId",
                table: "Payments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MeterDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    Value = table.Column<decimal>(nullable: false),
                    Difference = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(maxLength: 1000, nullable: true),
                    MeterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterDatas", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Payments_MeterDataId",
                table: "Payments",
                column: "MeterDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_MeterDatas_MeterDataId",
                table: "Payments",
                column: "MeterDataId",
                principalTable: "MeterDatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_MeterDatas_MeterDataId",
                table: "Payments");

            migrationBuilder.DropTable(
                name: "MeterDatas");

            migrationBuilder.DropIndex(
                name: "IX_Payments_MeterDataId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "MeterDataId",
                table: "Payments");

            migrationBuilder.AddColumn<int>(
                name: "IndicatorId",
                table: "Payments",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Indicators",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Description = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: true),
                    Difference = table.Column<decimal>(type: "numeric", nullable: false),
                    MeterId = table.Column<int>(type: "integer", nullable: false),
                    Value = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Indicators", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Payments_IndicatorId",
                table: "Payments",
                column: "IndicatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Indicators_IndicatorId",
                table: "Payments",
                column: "IndicatorId",
                principalTable: "Indicators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
