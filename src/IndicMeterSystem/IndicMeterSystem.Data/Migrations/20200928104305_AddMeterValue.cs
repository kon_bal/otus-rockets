﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class AddMeterValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Difference",
                table: "MeterDatas");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "MeterDatas");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "MeterDatas");

            migrationBuilder.CreateTable(
                name: "MeterValues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Number = table.Column<int>(nullable: false),
                    Value = table.Column<int>(nullable: false),
                    Difference = table.Column<int>(nullable: false),
                    MeterDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeterValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MeterValues_MeterDatas_MeterDataId",
                        column: x => x.MeterDataId,
                        principalTable: "MeterDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MeterValues_MeterDataId",
                table: "MeterValues",
                column: "MeterDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MeterValues");

            migrationBuilder.AddColumn<decimal>(
                name: "Difference",
                table: "MeterDatas",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Number",
                table: "MeterDatas",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "Value",
                table: "MeterDatas",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
