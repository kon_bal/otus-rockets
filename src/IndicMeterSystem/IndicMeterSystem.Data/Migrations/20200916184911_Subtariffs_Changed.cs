﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class Subtariffs_Changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SubTariffs_TariffId",
                table: "SubTariffs");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "SubTariffs");

            migrationBuilder.AddColumn<int>(
                name: "Number",
                table: "SubTariffs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_SubTariffs_TariffId_Number",
                table: "SubTariffs",
                columns: new[] { "TariffId", "Number" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_SubTariffs_TariffId_Number",
                table: "SubTariffs");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "SubTariffs");

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "SubTariffs",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_SubTariffs_TariffId",
                table: "SubTariffs",
                column: "TariffId");
        }
    }
}
