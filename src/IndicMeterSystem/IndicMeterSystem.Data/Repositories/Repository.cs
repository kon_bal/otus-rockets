﻿using IndicMeterSystem.Data.EF;
using IndicMeterSystem.Data.Interfaces;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IndicMeterSystem.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T :class
    {
        private ApplicationContext _db;
        private DbSet<T> _dbSet;

        public Repository(ApplicationContext context)
        {
            _db = context;
            _dbSet = context.Set<T>();
        }

        public async Task AddAsync(T item)
        {
            await _dbSet.AddAsync(item);
            await _db.SaveChangesAsync();
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet
                .AsNoTracking()
                .AnyAsync(predicate);
        }

        public async Task DeleteAsync(int id)
        {
            T item = await _dbSet.FindAsync(id);
            if (item != null)
                _dbSet.Remove(item);
            await _db.SaveChangesAsync();
        }

        public async Task<T> GetAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.AsNoTracking().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWithIncludeOnConditionAsync(string predicateIncl, 
            Expression<Func<T, bool>> predicateCond)
        {
            return await _dbSet
                .AsNoTracking()
                .Where(predicateCond)
                .Include(predicateIncl)
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWithIncludeOnConditionAsync(string predicateIncl1,
            string predicateIncl2, Expression<Func<T, bool>> predicateCond)
        {
            return await _dbSet
                .AsNoTracking()
                .Where(predicateCond)
                .Include(predicateIncl1)
                .Include(predicateIncl2)
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetOnConditionAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
        }

        public async Task UpdateAsync(T item)
        {
            _db.Entry(item).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        #region "Синхронные функции (Будут удалены)"
        [Obsolete("This method is obsolete. Use AddAsync instead.", false)]
        public void Add(T item)
        {
            _dbSet.Add(item);
            _db.SaveChanges();
        }

        [Obsolete("This method is obsolete. Use DeleteAsync instead.", false)]
        public void Delete(int id)
        {
            T item = _dbSet.Find(id);
            if (item != null)
                _dbSet.Remove(item);
            _db.SaveChanges();
        }

        [Obsolete("This method is obsolete. Use GetAsync instead.", false)]
        public T Get(int id)
        {
            return _dbSet.Find(id);
        }

        [Obsolete("This method is obsolete. Use GetAllAsync instead.", false)]
        public IEnumerable<T> GetAll()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        [Obsolete("This method is obsolete. Use GetOnConditionAsync instead.", false)]
        public IEnumerable<T> GetOnCondition(Func<T,bool> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }

        [Obsolete("This method is obsolete. Use UpdateAsync instead.", false)]
        public void Update(T item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }
        #endregion
    }
}
